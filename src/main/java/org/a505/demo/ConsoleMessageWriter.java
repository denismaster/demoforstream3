package org.a505.demo;

import org.springframework.stereotype.Service;

@Service
public class ConsoleMessageWriter implements MessageWriter {
    @Override
    public void write(String message) {
        System.out.println(message);
    }
}
