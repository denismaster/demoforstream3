package org.a505.demo;

public interface MessageWriter {
    void write(String message);
}
