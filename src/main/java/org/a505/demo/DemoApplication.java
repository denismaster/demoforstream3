package org.a505.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	private ApplicationContext appContext;

	public DemoApplication(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) {
		var salutation = this.appContext.getBean(Salutation.class);
		salutation.exclaim();
	}
}
