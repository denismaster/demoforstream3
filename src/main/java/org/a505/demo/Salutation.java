package org.a505.demo;

import org.springframework.stereotype.Service;

@Service
public class Salutation {
    private MessageWriter messageWriter;

    public Salutation(MessageWriter messageWriter) {
        if(messageWriter == null) {
            throw new IllegalArgumentException("messageWriter");
        }
        this.messageWriter = messageWriter;
    }

    public void exclaim() {
        this.messageWriter.write("Hello, Spring DI!");
    }
}
